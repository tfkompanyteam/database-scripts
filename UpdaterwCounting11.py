'''
Created on 1. kol 2014.
@author: Mihael
'''

import subprocess 
import MySQLdb
import csv
import time

states = ["NJ","NY","OH","OK","OR","PA","SC"];

inputCsvFileDict = {"NJ": "NJ_Business_Data_2013_1.csv",
                    "NY": "NY_Business_Data_2013_2.csv",
                    "OH": "OH_Business_Data_2013_1.csv",
                    "OK": "OK_Business_Data_2013.csv",
                    "OR": "OR_Business_Data_2013_1.csv",
                    "PA": "PA_Business_Data_2013_1.scv",
                    "SC": "SC_Business_Data_2013.csv"
                    }

for state in states:

    now = time.strftime('%Y-%m-%d %H:%M:%S')
    lastProcessed = 0
    current = 0
    try:
        command = "find /home/kskracic/us_handels_bulk/output/"+state+" -type f | wc -l"
        p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)      
        (stanje, err) = p.communicate()
        
        stanje = stanje[0:-1]   # removes '\n'
        stanje = stanje.replace(" ", "")
        
            #Priprema json-a
        jSon = '{count_output_files:'
        jSon += `stanje` 
        
        command = "ls -lrt /home/kskracic/us_handels_bulk/output/"+state+" | tail -1"
        p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)      
        (fileData, err) = p.communicate()

        fileData = fileData.split(" ")
        for data in fileData:
            if ".xml" in data:
                lastCompanieProcessed = data           
        
        lastCompanieProcessed = lastCompanieProcessed[0:-1]     # removes '\n'
        
        inputFile = "/home/kskracic/us_handels_bulk/input/"+inputCsvFileDict[state]
        
        with open(inputFile, 'rb') as csvfile:
            inputReader = csv.reader(csvfile, delimiter=',')
            for companyName in inputReader:
                current += 1
                companyName = companyName[0]        # csv delimiting bug
                if ',' in companyName:
                    companyName = companyName[0:companyName.find(',')]
                companyName = companyName.replace(";","")
                companyName = companyName.replace("'", "")
                companyName = companyName.replace("'", "") 
                companyName = companyName.replace(" ","_")
                companyName = companyName.replace("__","_")
                companyName = companyName.replace("&", "")
                companyName = companyName.replace("-","")
                companyName = companyName.replace(".","")
                companyName = companyName + ".xml"
                if companyName == lastCompanieProcessed:
                    lastProcessed = current
        
        jSon += ',total_lines:'
        jSon += `current`
        jSon += ',last_successfuly_processed_line:'
        jSon += `lastProcessed`
        jSon += '}'
        
        print jSon
        
        db = MySQLdb.connect(host="ec2-54-77-71-45.eu-west-1.compute.amazonaws.com", #  host
                             user="kompany", #  username
                              passwd="CnWnW73H", #  password
                              db="crawler") # name of the data base
        cursor = db.cursor()
        crawler = 'us_crawler' + state
        cursor.execute('''INSERT INTO `log_hotfix` (`id`, `application`, `machine`, `datetime`, `data`)
                            VALUES (%s, %s, %s, %s, %s)''',
                            (None, crawler, 'crawler11', now , jSon))
        db.commit()
        db.close()
    except:
        pass