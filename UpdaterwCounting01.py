'''
Created on 31. srp 2014.
@author: Mihael
'''

import subprocess  
import MySQLdb
import time
import csv

now = time.strftime('%Y-%m-%d %H:%M:%S')
current = 0

p = subprocess.Popen(["ls /home/kskracic/de_handels_bulk/output_xml | wc -l"], stdout=subprocess.PIPE, shell=True)
(stanje, err) = p.communicate()

stanje = stanje[0:-1]   # removes '\n'
stanje = stanje.replace(" ", "")

jSon = '{count_output_files:'
jSon += `stanje` 

inputFile = "/home/kskracic/de_handels_bulk/mappings.csv"
        
with open(inputFile, 'rb') as csvfile:
    inputReader = csv.reader(csvfile, delimiter=',')
    for companyName in inputReader:
        current += 1

jSon += ',total_lines:'
jSon += `current`
jSon += '}'

db = MySQLdb.connect(host="localhost", 
                     user="kompany", 
                      passwd="CnWnW73H", 
                      db="crawler") 
cursor = db.cursor()
cursor.execute('''INSERT INTO `log_hotfix` (`id`, `application`, `machine`, `datetime`, `data`)
                    VALUES (%s, %s, %s, %s, %s)''',
                    (None, 'de_crawler', 'crawler01', now , jSon))
db.commit()
db.close()


